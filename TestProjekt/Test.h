#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "Model/Model.h"

void processInput(GLFWwindow);
void errorCallback(int, const char*);
